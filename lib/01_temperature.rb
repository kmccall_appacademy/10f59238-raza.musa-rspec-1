def ftoc(ftemp)
 temperature = ((5.0/9.0) * ftemp) - (160.0/9)
 temperature
end

def ctof(ctemp)
  temperature = ((9.0/5.0) * ctemp) + 32
  temperature
end
