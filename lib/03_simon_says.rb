def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, val = 2)
  word_in_array = [string] * val
  word_in_array.join(" ")
end

def start_of_word(string, number)
  string[0...number]
end

def first_word(sentence)
  sentence.split[0]
end

def titleize(sentence)
  sentence_array = sentence.split
  sentence.capitalize if sentence_array.length == 1
  small_words = ["the", "and", "if", "over"]
  sentence_array.map.with_index do |word,idx|
    if idx == 0
      word.capitalize
    elsif small_words.include?(word)
      word
    else
      word.capitalize
    end
  end.join(" ")
end
