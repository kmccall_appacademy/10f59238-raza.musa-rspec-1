def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum(array)
  return 0 if array.empty?
  array.reduce(:+)
end

def multiply(array)
  array.reduce(:*)
end

def power(x, y)
  x**y
end

def factorial(number)
  return 1 if number == 0
  (1..number).reduce(:*)
end
