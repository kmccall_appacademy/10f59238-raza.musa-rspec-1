def translate(sentence)
  sentence.split.map {|word| latinize(word)}.join(" ")
end

def latinize(word)
  vowels = "aeioAEIO"
  if ("A".."Z").to_a.include?(word[0])
    until vowels.include?(word[0])
      word = word[1..-1] + word[0]
      word = word.capitalize
    end
    word + "ay"
  else
  until vowels.include?(word[0])
    word = word[1..-1] + word[0]
  end
  word + "ay"
  end
end
